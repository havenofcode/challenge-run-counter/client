import * as React from 'react';
import {Duration} from 'luxon';
import {Typography} from '@mui/material';

const addPadding = (num: string, addColon = true) => {
  const res = num.length === 1 ? `0${num}` : num;

  if (res === '') {
    return '';
  }

  if (addColon) {
    return `${res}:`;
  }

  return res;
}

export const renderDurationStringFromMillis = (millis: number, showSign = false) => {
  const absMillis = Math.abs(millis);
  const sign = millis < 0 ? '-' : '+';
  const obj = Duration.fromMillis(absMillis)
    .shiftTo('hours', 'minutes', 'seconds', 'milliseconds')
    .toObject();

  const hours = (obj.hours || 0) > 0 ? `${obj.hours}` : '';
  const minutes = `${obj.minutes}`;
  const seconds = `${obj.seconds}`;
  const fractionalSecond = Math.floor(obj.milliseconds ? obj.milliseconds / 10 : 0);
  const fract = `${fractionalSecond}`;
  return `${(millis && showSign) ? sign : ''}${hours}${hours.length > 0 ? ':' : ''}${hours.length > 0 ? addPadding(minutes, false) : minutes}:${addPadding(seconds, false)}.${addPadding(fract, false)}`;
}

interface IProps {
  activeAt: number;
  milliseconds: number;
  showSign?: boolean;
  type?: 'normal' | 'large';
}

interface IState {
  durationString: string;
}

export class DurationTimer extends React.Component<IProps, IState> {
  private timer: ReturnType<typeof setTimeout> = setTimeout(() => 69, 100);

  public state: IState = {
    durationString: '',
  }

  updateTime() {
    this.setState(
      {durationString: this.getTimeString()},
      () => {
        if (this.props.activeAt !== 0) {
          this.timer = setTimeout(() => this.updateTime(), 1000 / 60);
        }
      }
    );
  }

  componentDidMount() {
    this.updateTime();
  }

  componentWillUnmount() {
    clearTimeout(this.timer);
  }

  componentDidUpdate(prevProps: IProps) {
    if (
      prevProps.activeAt !== this.props.activeAt ||
      prevProps.milliseconds !== this.props.milliseconds
    ) {
      clearTimeout(this.timer);
      this.updateTime();
    }
  }

  getTimeString() {
    const {activeAt} = this.props;
    const currentMilliseconds = this.props.milliseconds;
    const addedTime = activeAt > 0 ? new Date().getTime() - activeAt : 0;
    const num = currentMilliseconds + addedTime;
    return renderDurationStringFromMillis(num, Boolean(this.props.showSign));
  }

  renderLargeType() {
    const str = this.getDurationString();
    const parts = str.split('.');
    return (
      <>
        <Typography
          variant="h3"
          sx={{
            fontFamily: 'timer',
            display: 'inline-block'
          }}
        >
          {parts[0] || ''}
        </Typography>
        <Typography
          variant="h5"
          sx={{
            fontFamily: 'timer',
            display: 'inline-block'
          }}
        >
          .{parts[1] || ''}
        </Typography>
      </>
    );
  }

  getDurationString() {
    if (this.props.activeAt > 0) {
      return this.state.durationString;
    }

    return renderDurationStringFromMillis(this.props.milliseconds, Boolean(this.props.showSign));
  }

  renderNormal() {
    return (
      <>
        {this.getDurationString()}
      </>
    );
  }

  render() {
    switch (this.props.type) {
      case 'large': {
        return this.renderLargeType();
      }
      case 'normal':
      default: {
        return this.renderNormal();
      }
    }
  }
}

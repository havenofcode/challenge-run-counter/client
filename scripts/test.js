/* eslint @typescript-eslint/no-var-requires: "off" */
const child_process = require('child_process');
const os = require('os');
const path = require('path');
const root = path.resolve(__dirname, '..');

const childProcessOptions = {
  cwd: root,
  stdio: 'inherit',
  env: {
    ...process.env,
    NODE_ENV: 'test',
  }
}

const waitUntilDone = async (args) => {
  const cmd = `> npx ${args.join(' ')}`;
  console.log(cmd);

  return new Promise((resolve) => {
    const child = child_process.spawn(
      ...([os.platform() === 'win32' ? 'npx.cmd' : 'npx']),
      args,
      childProcessOptions
    );

    child.on('exit', (code) => {
      if (code) {
        console.error(`error: ${cmd} exit with ${code}`);
        process.exit(code);
      } else {
        console.info(`success: ${cmd}`);
        resolve(code);
      }
    });
  });
}

const runLint = () => waitUntilDone(['npm', 'run', 'test:lint']);
const runTest = () => waitUntilDone(['npm', 'run', 'test:unit']);

const main = async () => {
  await runLint();
  await runTest();
}

main().catch((err) => {
  console.error(err);
  process.exit(1);
});

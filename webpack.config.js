const path = require('path');

module.exports = {
  entry: {
    main: './dist/index'
  },
  mode: process.env.NODE_ENV === 'production' ? 'production' : 'development',
  output: {
    filename: 'challenge-run-client.min.js',
    path: path.resolve(__dirname, 'dist'),
    libraryTarget: 'var',
    library: 'ChallengeRunClient',
  },
};

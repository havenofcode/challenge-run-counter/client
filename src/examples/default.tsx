import React from 'react';
import ReactDOM from 'react-dom';

import {
  CssBaseline,
  ThemeProvider,
  Paper,
  Box,
} from '@mui/material';

import {WithTheme, withTheme} from '@mui/styles';
import {Theme} from '@mui/material/styles';
import {css, cx} from '@emotion/css';
import {createTheme} from '@mui/material/styles';
import {connect, Client} from '../client';

import {
  ISplitsData,
  ISplitItem,
} from '../client';

const theme = createTheme({
  palette: {
    mode: 'dark',
  },
});

import {DurationTimer} from '../components/duration_timer';

type IProps = WithTheme<Theme>;

export interface IDefaultState {
  splitsData?: ISplitsData;
  errorMessage?: string;
  width: string | number;
  height: string | number;
}

interface DefaultViewerSettings {
  show_num_next_splits: number;
  show_num_prev_splits: number;
}

export class DefaultAppImpl extends React.Component<IProps, IDefaultState> {
  state: IDefaultState = {
    width: "auto",
    height: "auto",
  }

  private client?: Client;

  constructor(props: IProps) {
    super(props);
  }

  getUserDefinedDimension(num: string) {
    if (num.trim() === 'auto' || num.trim() === '') {
      return '100%';
    }

    let isNumber = true;
    for (let i = 0; i < num.length; ++i) {
      const c = num.charAt(i);

      // if it's not a number
      if (!(c >= '0' && c <= '9')) {
        isNumber = false;
        break;
      }
    }

    return `${num}${isNumber ? 'px' : ''}`;
  }

  splitsUpdated(splitsData: ISplitsData) {
    this.setState({
      width: this.getUserDefinedDimension(splitsData.pageSettings.width),
      height: this.getUserDefinedDimension(splitsData.pageSettings.height),
      splitsData,
    });
  }

  async componentDidMount() {
    try {
      this.client = await connect({
        host: 'localhost',
        port: '42069',
        app_name: 'default.html',
      });

      this.client.on('splits-updated', this.splitsUpdated.bind(this));

      this.client.emit(
        'notify-configuration-available',
        {
          name: 'default.html',
          configuration: {
            show_num_prev_splits: {
              type: 'number',
              label: 'Show Next N Splits',
              helperText: 'The maximum number upcoming splits to show',
              min: 0,
              max: 99,
            },
            show_num_next_splits: {
              type: 'number',
              label: 'Show Previous M Splits',
              helperText: 'The maximum number previous splits to show',
              min: 0,
              max: 99,
            },
          },
        },
        (err?: Error) => {
          console.log('ERROR?', err);
        },
      );
    } catch (err) {
      console.error(err);
      const error = err as Error;
      this.setState({errorMessage: error.message});
    }
  }

  async componentWillUnmount() {
    if (this.client) {
      this.client.disconnect();
      delete this.client;
    }
  }

  private s = {
    errorColor: css`
      color: ${this.props.theme.palette.error.light} !important;
    `,
    successColor: css`
      color: ${this.props.theme.palette.success.light} !important;
    `,
    splitsWrapper: css`
      position: absolute;
      overflow: hidden;
    `,
    clearBackground: css`
      background-color: rgba(0, 0, 0, 0) !important;
    `,
    headerBackground: css`
      background-color: #121212;
      background-image: linear-gradient(rgba(255, 255, 255, 0.05), rgba(255, 255, 255, 0.05));
    `
  }

  renderSplitDuration(id: number) {
    if (!this.state.splitsData) {
      return null;
    }

    const splitDuration = this.state.splitsData.attemptDuration.splits[id];

    if (!splitDuration) {
      return;
    }

    return (
      <Box sx={{pl: 1}}>
        <DurationTimer
          milliseconds={splitDuration.milliseconds}
          activeAt={splitDuration.activeAt}
        />
      </Box>
    );
  }

  getViewerSettings(): DefaultViewerSettings {
    const defaultViewerSettings = {
      show_num_next_splits: 2,
      show_num_prev_splits: 4,
    }

    if (!this.state.splitsData) {
      return defaultViewerSettings;
    }

    const {pageSettings} = this.state.splitsData;

    if (!pageSettings.viewerSettings['default.html']) {
      return defaultViewerSettings;
    }

    const {
      show_num_next_splits,
      show_num_prev_splits,
    } = (pageSettings.viewerSettings['default.html'] as unknown as DefaultViewerSettings);

    if (
      show_num_next_splits &&
      show_num_next_splits > 0
    ) {
      defaultViewerSettings.show_num_next_splits = show_num_next_splits;
    }

    if (
      show_num_prev_splits &&
      show_num_prev_splits > 0
    ) {
      defaultViewerSettings.show_num_prev_splits = show_num_prev_splits;
    }

    return defaultViewerSettings;
  }

  renderSplits() {
    if (!this.state.splitsData) {
      return null;
    }

    const {activeSplits} = this.state.splitsData;
    const {show_num_next_splits, show_num_prev_splits} = this.getViewerSettings();
    let indexActive = 0;

    for (let i = 0; i < activeSplits.length; ++i) {
      const split = activeSplits[i];
      if (split.active) {
        indexActive = i;
        break;
      }
    }

    let startingIndex = 0;
    const maxSplitsShown = show_num_next_splits + show_num_prev_splits + 1;

    if (indexActive <= show_num_prev_splits || activeSplits.length <= maxSplitsShown) {
      startingIndex = 0;
    } else if (indexActive >= (activeSplits.length - 1) - show_num_next_splits) {
      startingIndex = activeSplits.length - maxSplitsShown;
    } else {
      startingIndex = indexActive - show_num_prev_splits;
    }

    const splitsShown = activeSplits.slice(
      startingIndex,
      startingIndex + maxSplitsShown
    );

    let afterCurrentSplit = false;

    return splitsShown.map((split) => {
      const row = this.renderSplitsRow(split, afterCurrentSplit);

      if (!afterCurrentSplit) {
        afterCurrentSplit = split.active;
      }

      return row;
    });

  }

  renderAttemptDuration() {
    if (!this.state.splitsData) {
      return null;
    }

    const attemptDuration = this.state.splitsData.attemptDuration;

    if (
      attemptDuration.attempt === 0 &&
      attemptDuration.attemptActiveAt === 0
    ) {
      return null;
    }

    return (
      <Box sx={{pl: 1}}>
        <DurationTimer
          milliseconds={attemptDuration.attempt}
          activeAt={attemptDuration.attemptActiveAt}
        />
      </Box>
    );
  }

  renderHeaderRow() {
    const {s} = this;
    return (
      <>
        <Box
          className={cx("head-row-start", s.headerBackground)}
          sx={{zIndex: 1, pl: 1}}
        >
          <Box sx={{display: 'flex', mr: 1}}>
            <Box sx={{flexGrow: 1}}>
              Split
            </Box>
          </Box>
        </Box>
        <Box
          className={s.headerBackground}
          sx={{zIndex: 1, pl: 1, textAlign: 'right'}}
        >
          Now
        </Box>
        <Box
          className={s.headerBackground}
          sx={{zIndex: 1, pl: 1, textAlign: 'right'}}
        >
          Diff
        </Box>
        <Box
          className={s.headerBackground}
          sx={{zIndex: 1, pl: 1}}
        >
          PB
        </Box>
      </>
    );
  }

  renderSplitsRow(splitStep: ISplitItem, afterCurrentSplit: boolean) {
    if (!this.state.splitsData) {
      return null;
    }

    const {id, name, hits_way, hits_boss, pb_hits} = splitStep;
    const {s} = this;
    let color = '';

    if (hits_way + hits_boss !== 0) {
      color = s.errorColor;
    } else if (!afterCurrentSplit) {
      color = s.successColor;
    }

    const diff = hits_way + hits_boss - (pb_hits || 0);

    return (
      <React.Fragment key={`splits_row_fragment_${splitStep.id}`}>
        <Box
          className={`row-start ${color}`}
          sx={{pl: 1}}
        >
          <Box sx={{display: 'flex', mr: 1}}>
            <Box sx={{
              flexGrow: 1,
              textOverflow: 'ellipsis',
              whiteSpace: 'nowrap'
            }}>
              {name}
            </Box>
            {this.renderSplitDuration(id)}
          </Box>
        </Box>
        <Box
          className={color}
          sx={{textAlign: 'right', pl: 1}}
        >
          {hits_way + hits_boss}
        </Box>
        <Box
          className={color}
          sx={{textAlign: 'right', pl: 1}}
        >
          {pb_hits !== null && diff}
        </Box>
        <Box
          className={color}
          sx={{pl: 1}}
        >
          {(pb_hits || 0)}
        </Box>
      </React.Fragment>
    );
  }

  renderFooterRow() {
    if (!this.state.splitsData) {
      return null;
    }

    const {activeSplits} = this.state.splitsData;
    let totalCurrentHits = 0;
    let totalPB = 0;

    for (let i = 0; i < activeSplits.length; ++i) {
      const split = activeSplits[i];
      if (split.pb_hits) {
        totalPB += split.pb_hits;
      }
      if (split.hits_way) {
        totalCurrentHits += split.hits_way;
      }
      if (split.hits_boss) {
        totalCurrentHits += split.hits_boss;
      }
    }

    return (
      <>
        <Box
          className="footer-row"
          sx={{pl: 1}}
        >
          <Box sx={{display: 'flex', mr: 1}}>
            <Box sx={{flexGrow: 1}}>
              Total
            </Box>
            {this.renderAttemptDuration()}
          </Box>
        </Box>
        <Box
          sx={{zIndex: 1, pl: 1, textAlign: 'right'}}
        >
          {totalCurrentHits}
        </Box>
        <Box
          sx={{zIndex: 1, pl: 1, textAlign: 'right'}}
        >
        </Box>
        <Box
          sx={{zIndex: 1, pl: 1}}
        >
          {totalPB}
        </Box>
      </>
    );
  }

  renderTable() {
    if (!this.state.splitsData) {
      return null;
    }

    if (!this.state.splitsData) {
      return;
    }

    const {width, height} = this.state;

    return (
      <Box
        sx={{
          position: 'absolute',
          left: 0,
          top: 0,
          width: 1,
          height: 1,
          paddingTop: '10px',
        }}
      >
        <Box
          className={'challenge-run-splits-grid'}
          sx={{
            display: 'grid',
            gridTemplateColumns: '1fr 1fr 1fr 1fr',
            gridAutoRows: 'min-content',
            width,
            height,
          }}
          component={Paper}
        >
          {this.renderHeaderRow()}
          {this.renderSplits()}
          {this.renderFooterRow()}
        </Box>
      </Box>
    );
  }

  renderProgressBar() {
    if (!this.state.splitsData) {
      return null;
    }

    let indexActive = 0;

    const {activeSplits} = this.state.splitsData;
    for (let i = 0; i < activeSplits.length; ++i) {
      const split = activeSplits[i];
      if (split.active) {
        indexActive = i;
        break;
      }
    }

    return (
      <Box
        sx={{
          position: 'absolute',
          left: 0,
          top: 0,
          height: '10px',
          width: 1,
          zIndex: 1,
        }}
        square
        component={Paper}
        elevation={2}
      >
        <Box
          sx={{
            width:  (indexActive + 1) / activeSplits.length,
            position: 'absolute',
            left: 0,
            top: 0,
            height: 1,
          }}
          component={Paper}
          elevation={24}
          square
        />
      </Box>
    );
  }

  render() {
    if (!this.state.splitsData) {
      return null;
    }

    const {css} = this.state.splitsData.pageSettings;
    const {s} = this;

    return (
      <Box
        className={s.splitsWrapper}
        sx={{
          width: this.state.width,
          height: this.state.height,
        }}
      >
        <style dangerouslySetInnerHTML={{__html: css}} />
        {this.renderTable()}
        {this.renderProgressBar()}
      </Box>
    );
  }
}

export const DefaultApp = withTheme<Theme, typeof DefaultAppImpl>(DefaultAppImpl);

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <CssBaseline />
    <DefaultApp />
  </ThemeProvider>,
  document.querySelector('#root'),
);

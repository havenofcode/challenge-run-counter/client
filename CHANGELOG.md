CHANGELOG
=========

## [1.1.18](https://gitlab.com/havenofcode/challenge-run-counter/client/compare/v1.1.17...v1.1.18) (2022-01-07)


### Bug Fixes

* index modular settings by the app name and settings to speed run example ([653c15f](https://gitlab.com/havenofcode/challenge-run-counter/client/commit/653c15fa22149e09a75ed0095aa0998e966620cb))

## [1.1.17](https://gitlab.com/havenofcode/challenge-run-counter/client/compare/v1.1.16...v1.1.17) (2022-01-03)


### Bug Fixes

* use modular settings configuration api ([48bf4f9](https://gitlab.com/havenofcode/challenge-run-counter/client/commit/48bf4f9f98b75cae639179f7e2ec6db879d27745))

## [1.1.16](https://gitlab.com/havenofcode/challenge-run-counter/client/compare/v1.1.15...v1.1.16) (2022-01-02)


### Bug Fixes

* add progress indicator for default.html ([8328fc8](https://gitlab.com/havenofcode/challenge-run-counter/client/commit/8328fc8a1c70b1575f323827822dbf583af45530))
* refactor autoscroll and render footer with totals ([bce6b74](https://gitlab.com/havenofcode/challenge-run-counter/client/commit/bce6b74bb3b19f8d0e82d89aca5b383a55634f0e))

## [1.1.15](https://gitlab.com/havenofcode/challenge-run-counter/client/compare/v1.1.14...v1.1.15) (2021-12-29)


### Bug Fixes

* enable long polling for socket-io ([24e8a00](https://gitlab.com/havenofcode/challenge-run-counter/client/commit/24e8a003bd0e9906fe0014d719db6d394712353d))
* install dev dependencies explicitly for build ([8357856](https://gitlab.com/havenofcode/challenge-run-counter/client/commit/83578561617c1d7ffdfa6b3e6af6d6e343baf5ff))

## [1.1.14](https://gitlab.com/havenofcode/challenge-run-counter/client/compare/v1.1.13...v1.1.14) (2021-12-26)


### Bug Fixes

* use React.Fragment to render array of arrays ([064b8d2](https://gitlab.com/havenofcode/challenge-run-counter/client/commit/064b8d2e0faa3df5c7fc4f129e9511b4bb4ffb27))

## [1.1.13](https://gitlab.com/havenofcode/challenge-run-counter/client/compare/v1.1.12...v1.1.13) (2021-12-24)


### Bug Fixes

* let socket-io handle reconnect logic ([187a56a](https://gitlab.com/havenofcode/challenge-run-counter/client/commit/187a56abeef5f3d27d43a2dad1c5e930b97c7cdc))

## [1.1.12](https://gitlab.com/havenofcode/challenge-run-counter/client/compare/v1.1.11...v1.1.12) (2021-12-24)


### Bug Fixes

* refactor server-restart-now to reload browser clients and notify for commonjs clients ([596dc31](https://gitlab.com/havenofcode/challenge-run-counter/client/commit/596dc311998bbd03a25354705af070b3f377236d))

## [1.1.11](https://gitlab.com/havenofcode/challenge-run-counter/client/compare/v1.1.10...v1.1.11) (2021-12-21)


### Bug Fixes

* restart browser or kill process if client detects new websocket api version ([f78c110](https://gitlab.com/havenofcode/challenge-run-counter/client/commit/f78c110fb88e3ed3b9192c1318e7efee7b08b816))

## [1.1.10](https://gitlab.com/havenofcode/challenge-run-counter/client/compare/v1.1.9...v1.1.10) (2021-12-18)


### Bug Fixes

* remove heartbeat, and add debug logs temporarily ([428f5a2](https://gitlab.com/havenofcode/challenge-run-counter/client/commit/428f5a2a9c833fca84fef8a0dd96da1fd7891ba4))

## [1.1.9](https://gitlab.com/havenofcode/challenge-run-counter/client/compare/v1.1.8...v1.1.9) (2021-12-08)


### Bug Fixes

* finish speed run example ([f8efa7e](https://gitlab.com/havenofcode/challenge-run-counter/client/commit/f8efa7ea180bb8475b0217f1b11d8c5df449d5a7))

## [1.1.8](https://gitlab.com/havenofcode/challenge-run-counter/client/compare/v1.1.7...v1.1.8) (2021-12-07)


### Bug Fixes

* add speed run example ([bd40493](https://gitlab.com/havenofcode/challenge-run-counter/client/commit/bd404939542cf21733ca23dc36ecd1355713d4ef))

## [1.1.7](https://gitlab.com/havenofcode/challenge-run-counter/client/compare/v1.1.6...v1.1.7) (2021-12-06)


### Bug Fixes

* do not duplicate pb ([26e9c43](https://gitlab.com/havenofcode/challenge-run-counter/client/commit/26e9c43910c99574c9be7f74e8764e26cb381f77))

## [1.1.6](https://gitlab.com/havenofcode/challenge-run-counter/client/compare/v1.1.5...v1.1.6) (2021-12-06)


### Bug Fixes

* page blank on new install ([bf89857](https://gitlab.com/havenofcode/challenge-run-counter/client/commit/bf89857847450174b9c4e93fce63139cfc269c5c))

## [1.1.5](https://gitlab.com/havenofcode/challenge-run-counter/client/compare/v1.1.4...v1.1.5) (2021-12-05)


### Bug Fixes

* timer overwriting actual time ([f9000d5](https://gitlab.com/havenofcode/challenge-run-counter/client/commit/f9000d595a9319d971978eaf743096d14662ed6d))

## [1.1.4](https://gitlab.com/havenofcode/challenge-run-counter/client/compare/v1.1.3...v1.1.4) (2021-12-04)


### Bug Fixes

* delay removed when increment from zero for counting ([f8d8ccf](https://gitlab.com/havenofcode/challenge-run-counter/client/commit/f8d8ccfdb91a2d1fcb5a16ec33ed4ff39aea071c))

## [1.1.3](https://gitlab.com/havenofcode/challenge-run-counter/client/compare/v1.1.2...v1.1.3) (2021-12-04)


### Bug Fixes

* listen for page-settings-update for appearance changes ([308cf93](https://gitlab.com/havenofcode/challenge-run-counter/client/commit/308cf9313fe216c84e346840ea520f050de33260))

## [1.1.2](https://gitlab.com/havenofcode/challenge-run-counter/client/compare/v1.1.1...v1.1.2) (2021-12-02)


### Bug Fixes

* disconnect from server when requested ([c082905](https://gitlab.com/havenofcode/challenge-run-counter/client/commit/c082905a32b84425bea3bbfd66754dfcf0bfb756))

## [1.1.1](https://gitlab.com/havenofcode/challenge-run-counter/client/compare/v1.1.0...v1.1.1) (2021-11-30)


### Bug Fixes

* expose challenge-run-client in non-node environment and add docs ([e36a173](https://gitlab.com/havenofcode/challenge-run-counter/client/commit/e36a173578345030b8af84564fc0d677e56ea8c9))

# [1.1.0](https://gitlab.com/havenofcode/challenge-run-counter/client/compare/v1.0.1...v1.1.0) (2021-11-29)


### Bug Fixes

* show better time format ([3a9cf03](https://gitlab.com/havenofcode/challenge-run-counter/client/commit/3a9cf03cb9822771308fa03df904c593790b675f))


### Features

* add autoscrolling and render timer ([b897257](https://gitlab.com/havenofcode/challenge-run-counter/client/commit/b897257e16127107aee549439db123291b9c1488))

## [1.0.1](https://gitlab.com/havenofcode/challenge-run-counter/client/compare/v1.0.0...v1.0.1) (2021-11-01)


### Bug Fixes

* usable in node and handle other events ([cb05b35](https://gitlab.com/havenofcode/challenge-run-counter/client/commit/cb05b35c026593c12584e96d1121801b41cca747))

# 1.0.0 (2021-10-30)


### Bug Fixes

* add description to npm package ([5c48526](https://gitlab.com/havenofcode/challenge-run-counter/client/commit/5c48526a00f2756affc626093cf81dafbd32be5a))
* add examplese folder to bundle ([6008766](https://gitlab.com/havenofcode/challenge-run-counter/client/commit/60087669f8a3f7e0a53cea8357eafd2f7bd015c4))
* build before publish ([dbd56e5](https://gitlab.com/havenofcode/challenge-run-counter/client/commit/dbd56e55b608ce5fcf25f7415428b0a02d677d7a))
* first test run of semantic-release ([b5a0e02](https://gitlab.com/havenofcode/challenge-run-counter/client/commit/b5a0e02ea20ced0574cf0b7608e6ab2e5f3f7404))
* package.json should point to official git repo ([30cc450](https://gitlab.com/havenofcode/challenge-run-counter/client/commit/30cc4500700d1ff44c432ce77daf1ea1ce9fb3c4))
* publish first release ([3898808](https://gitlab.com/havenofcode/challenge-run-counter/client/commit/38988087fb80f419fcb442fe9751a618dce03b4b))
* publish to official npm registry ([296a659](https://gitlab.com/havenofcode/challenge-run-counter/client/commit/296a65909f7509275c119f3dd4ecfda9a371c2d1))
* remove .npmrc causing publish to internal repo ([fada197](https://gitlab.com/havenofcode/challenge-run-counter/client/commit/fada1978f3d4c9e860744562b7113bbed6d7caae))
* semantic release should publish changelog ([46bedd7](https://gitlab.com/havenofcode/challenge-run-counter/client/commit/46bedd701cdf1b78ef532b23239ea3f0e2f69e11))

# [1.0.0-beta.3](https://gitlab.com/havenofcode/challenge-run-counter/client/compare/v1.0.0-beta.2...v1.0.0-beta.3) (2021-10-30)


### Bug Fixes

* add description to npm package ([5c48526](https://gitlab.com/havenofcode/challenge-run-counter/client/commit/5c48526a00f2756affc626093cf81dafbd32be5a))

# [1.0.0-beta.2](https://gitlab.com/havenofcode/challenge-run-counter/client/compare/v1.0.0-beta.1...v1.0.0-beta.2) (2021-10-30)


### Bug Fixes

* remove .npmrc causing publish to internal repo ([fada197](https://gitlab.com/havenofcode/challenge-run-counter/client/commit/fada1978f3d4c9e860744562b7113bbed6d7caae))

# 1.0.0-beta.1 (2021-10-30)


### Bug Fixes

* add examplese folder to bundle ([6008766](https://gitlab.com/havenofcode/challenge-run-counter/client/commit/60087669f8a3f7e0a53cea8357eafd2f7bd015c4))
* build before publish ([dbd56e5](https://gitlab.com/havenofcode/challenge-run-counter/client/commit/dbd56e55b608ce5fcf25f7415428b0a02d677d7a))
* first test run of semantic-release ([b5a0e02](https://gitlab.com/havenofcode/challenge-run-counter/client/commit/b5a0e02ea20ced0574cf0b7608e6ab2e5f3f7404))
* package.json should point to official git repo ([30cc450](https://gitlab.com/havenofcode/challenge-run-counter/client/commit/30cc4500700d1ff44c432ce77daf1ea1ce9fb3c4))
* publish to official npm registry ([296a659](https://gitlab.com/havenofcode/challenge-run-counter/client/commit/296a65909f7509275c119f3dd4ecfda9a371c2d1))
* semantic release should publish changelog ([46bedd7](https://gitlab.com/havenofcode/challenge-run-counter/client/commit/46bedd701cdf1b78ef532b23239ea3f0e2f69e11))

# [1.0.0-beta.3](https://source.havenofcode.com/havenofcode/challenge-run-viewer/compare/v1.0.0-beta.2...v1.0.0-beta.3) (2021-10-30)


### Bug Fixes

* build before publish ([dbd56e5](https://source.havenofcode.com/havenofcode/challenge-run-viewer/commit/dbd56e55b608ce5fcf25f7415428b0a02d677d7a))

# [1.0.0-beta.2](https://source.havenofcode.com/havenofcode/challenge-run-viewer/compare/v1.0.0-beta.1...v1.0.0-beta.2) (2021-10-30)


### Bug Fixes

* add examplese folder to bundle ([6008766](https://source.havenofcode.com/havenofcode/challenge-run-viewer/commit/60087669f8a3f7e0a53cea8357eafd2f7bd015c4))

# 1.0.0-beta.1 (2021-10-30)


### Bug Fixes

* first test run of semantic-release ([b5a0e02](https://source.havenofcode.com/havenofcode/challenge-run-viewer/commit/b5a0e02ea20ced0574cf0b7608e6ab2e5f3f7404))

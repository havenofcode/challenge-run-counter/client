#!/usr/bin/env node
/* eslint-disable @typescript-eslint/no-var-requires */
const {Command} = require('commander');
const program = new Command();

program
  .version('1.0.0')
  .description('challenge-run-client cli example')
  .command('print', 'prints out data and exits').alias('p');

program.parse(process.argv);

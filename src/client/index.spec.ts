import {EventEmitter} from 'events';
import {expect} from 'chai';
import {createSandbox, SinonSpy, SinonStub} from 'sinon';
import {Socket} from 'socket.io-client';
import {connect, Client, ClientState} from './';
import {ISplitsData} from '..';
import mockOnConnectInitJson from '../test/fixtures/mock-splits-updated-init.json';
const mockOnConnectInitResponse = mockOnConnectInitJson as ISplitsData;

describe('client/index', () => {
  const sandbox = createSandbox();

  beforeEach(() => {
    sandbox.restore();
    sandbox.stub(process, 'exit');

    class FakeClient extends EventEmitter {
      connect = sandbox.spy();
      disconnect = sandbox.spy();
      disconnected = false;
      io = {on: sandbox.spy()};
    }

    sandbox.stub(Client, 'ioclient').returns(new FakeClient() as unknown as Socket);
  });

  it('exists', () => {
    expect(connect).to.be.a('function');
  });

  it('stubs connect call', async () => {
    console.log('try');
    const connection = await connect();
    console.log('no?');
    expect(connection.socket.disconnected).to.eql(false);
    expect(connection.socket.connect).to.be.a('function');
    expect(connection.socket.disconnect).to.be.a('function');
    expect(connection.socket.emit).to.be.a('function');
    expect((connection.socket.connect as SinonSpy).getCall(0)).to.be.null;
    expect(connection.state).to.eql(ClientState.CLIENT_CONNECTING);
  });

  it('set state on successful connection', async () => {
    const connection = await connect();
    expect(connection.state).to.eql(ClientState.CLIENT_CONNECTING);
    connection.socket.emit('connect');
    expect(connection.state).to.eql(ClientState.CLIENT_OK);
  });

  it('stubs process.exit', () => {
    process.exit(420);
    expect((process.exit as unknown as SinonSpy).getCalls()).to.have.lengthOf(1);
  });

  it('remembers api_version on connect', async () => {
    const connection = await connect();
    const splitEvents: ISplitsData[] = [];
    connection.on('splits-updated', (data: ISplitsData) => {
      splitEvents.push(data);
    });
    connection.socket.emit('connect');
    expect(connection.lastApiVersionConnected).to.be.undefined;
    expect((process.exit as unknown as SinonStub).getCalls()).to.have.lengthOf(0);
    connection.socket.emit('splits-updated', mockOnConnectInitResponse);
    expect(connection.lastApiVersionConnected).to.eql('1.4.13');
    expect((process.exit as unknown as SinonStub).getCalls()).to.have.lengthOf(0);
    connection.socket.emit('splits-updated', mockOnConnectInitResponse);
    connection.socket.emit(
      'splits-updated',
      {mockOnConnectInitResponse, api_version: 'banana'}
    );
    expect(splitEvents).to.have.lengthOf(2);
    expect((process.exit as unknown as SinonStub).getCalls()).to.have.lengthOf(1);
    expect((process.exit as unknown as SinonStub).getCall(0).args[0]).to.eql(69);
  });
});

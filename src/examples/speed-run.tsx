
import React from 'react';
import ReactDOM from 'react-dom';

import {
  CssBaseline,
  ThemeProvider,
  Paper,
  Box,
  Typography,
} from '@mui/material';

import {WithTheme, withTheme} from '@mui/styles';
import {Theme} from '@mui/material/styles';
import {css, cx} from '@emotion/css';
import {connect, Client} from '../client';
import {createTheme} from '@mui/material/styles';

const theme = createTheme({
  palette: {
    mode: 'dark',
  },
});

import {
  ISplitsData,
  ISplitItem,
} from '../client';

import {DurationTimer} from '../components/duration_timer';

type IProps = WithTheme<Theme>;

export interface ISpeedRunState {
  splitsData?: ISplitsData;
  errorMessage?: string;
  width: string | number;
  height: string | number;
}

export class SpeedRunAppImpl extends React.Component<IProps, ISpeedRunState> {
  state: ISpeedRunState = {
    width: "auto",
    height: "auto",
  }

  private client?: Client;
  private containerRef = React.createRef<HTMLDivElement>();
  private rowMap: {[id: number]: React.RefObject<HTMLDivElement | null>[]} = {};
  private headerRowRefs: React.RefObject<HTMLDivElement | null>[] = [];
  private maxRows = 7;
  private bottomRows = 3;

  constructor(props: IProps) {
    super(props);
  }

  getUserDefinedDimension(num: string) {
    if (num.trim() === 'auto' || num.trim() === '') {
      return '100%';
    }

    let isNumber = true;
    for (let i = 0; i < num.length; ++i) {
      const c = num.charAt(i);

      // if it's not a number
      if (!(c >= '0' && c <= '9')) {
        isNumber = false;
        break;
      }
    }

    return `${num}${isNumber ? 'px' : ''}`;
  }

  splitsUpdated(splitsData: ISplitsData) {
    this.setState({
      width: this.getUserDefinedDimension(splitsData.pageSettings.width),
      height: this.getUserDefinedDimension(splitsData.pageSettings.height),
      splitsData,
    });
  }

  async componentDidMount() {
    try {
      this.client = await connect();
      this.client.on('splits-updated', this.splitsUpdated.bind(this));

      this.client.emit(
        'notify-configuration-available',
        {
          name: 'speed-run.html',
          configuration: {
            logo_image: {
              type: 'file',
              label: 'Logo Image',
              helperText: 'Add a logo to display on your splits!',
            },
          },
        },
        (err?: Error) => {
          console.log('ERROR?', err);
        },
      );
    } catch (err) {
      console.error(err);
      const error = err as Error;
      this.setState({errorMessage: error.message});
    }
  }

  async componentWillUnmount() {
    if (this.client) {
      this.client.disconnect();
      delete this.client;
    }
  }

  renderSplitDuration(split: ISplitItem, addMillis = 0) {
    if (!this.state.splitsData) {
      return null;
    }
    const {attemptDuration} = this.state.splitsData;
    if (!attemptDuration.splits[split.id]) {
      return null;
    }

    if (split.active) {
      return (
        <DurationTimer
          milliseconds={attemptDuration.attempt}
          activeAt={attemptDuration.attemptActiveAt}
        />
      );
    }

    return (
      <DurationTimer
        milliseconds={attemptDuration.splits[split.id].milliseconds + addMillis}
        activeAt={attemptDuration.splits[split.id].activeAt}
      />
    );
  }

  renderSplits() {
    if (!this.state.splitsData) {
      return;
    }

    const splitEndTime: {[key: number]: number} = {}
    const splitAddTime: {[key: number]: number} = {}
    const splitPbTotal: {[key: number]: number} = {}
    const {attemptDuration} = this.state.splitsData;

    let addMillis = 0;
    let idxActive = 0;
    let pbTotal = 0;
    for (let i = 0; i < this.state.splitsData.activeSplits.length; ++i) {
      const split = this.state.splitsData.activeSplits[i];
      pbTotal += split.pb_time || 0;
      splitPbTotal[split.id] = pbTotal;

      if (split.active) {
        idxActive = i;
        break;
      }

      const duration = attemptDuration.splits[split.id];
      if (duration) {
        splitAddTime[split.id] = addMillis;
        addMillis += duration.milliseconds;
        splitEndTime[split.id] = addMillis;
      }
    }

    const len = this.state.splitsData.activeSplits.length;
    const factor = this.maxRows - this.bottomRows;
    let offsetFrom = 0;
    let offsetTo = this.maxRows;

    if ((idxActive + 1) - factor > 0 && idxActive + 1 + this.bottomRows < len) {
      offsetFrom = idxActive - factor;
      offsetTo = idxActive + this.bottomRows;
    } else if (len > this.maxRows && idxActive + 1 + this.bottomRows >= len) {
      offsetFrom = len - this.maxRows;
      offsetTo = len;
    }

    return this.state.splitsData.activeSplits.slice(offsetFrom, offsetTo).map((split) => {
      return (
        <React.Fragment key={`split_row_fragment_${split.id}`}>
          <Box
            className={cx({[this.s.splitActive]: split.active})}
            sx={{
              pr: 1,
              pl: 1,
              fontFamily: "'Fira Sans'",
              whiteSpace: 'nowrap',
              textOverflow: 'ellipsis',
              overflow: 'hidden',
            }}
          >
            {split.name}
          </Box>
          <Box
            className={cx({
              [this.s.splitActive]: split.active,
            })}
          >
            <Box
              className={cx({
                [this.s.blueTextGradient]: split.active,
                [this.s.redTextGradient]: !split.active && (splitEndTime[split.id] - splitPbTotal[split.id] > 0),
                [this.s.greenTextGradient]: !split.active && (splitEndTime[split.id] - splitPbTotal[split.id] < 0),
              })}
              sx={{
                textAlign: 'right',
                pr: 1,
                fontFamily: "timer",
                whiteSpace: 'nowrap',
              }}
            >
              {this.renderSplitDuration(split, splitAddTime[split.id] || 0)}
            </Box>
          </Box>
        </React.Fragment>
      );
    });
  }

  private s = {
    errorColor: css`
      color: ${this.props.theme.palette.error.light} !important;
    `,
    greenTextGradient: css`
      background: linear-gradient(180deg, rgba(128,255,162,1) 35%, rgba(0,163,44,1) 100%);
      -webkit-background-clip: text;
      -webkit-text-fill-color: transparent;
    `,
    blueTextGradient: css`
      background: linear-gradient(180deg, rgba(138, 210, 255,1) 35%, rgba(16, 132, 204,1) 100%);
      -webkit-background-clip: text;
      -webkit-text-fill-color: transparent;
    `,
    redTextGradient: css`
      background: linear-gradient(180deg, rgba(255, 128, 128,1) 35%, rgba(163, 0, 0,1) 100%);
      -webkit-background-clip: text;
      -webkit-text-fill-color: transparent;
    `,
    grayTextGradient: css`
      background: linear-gradient(180deg, rgba(191, 191, 191, 1) 35%, rgba(82, 82, 82, 1) 100%);
      -webkit-background-clip: text;
      -webkit-text-fill-color: transparent;
    `,
    splitActive: css`
      background-color: ${this.props.theme.palette.action.hover}
    `
  }

  getTimerColor() {
    if (!this.state.splitsData) {
      return this.s.grayTextGradient;
    }

    const {activeRoute, attemptDuration} = this.state.splitsData;

    if (!attemptDuration.attemptActiveAt) {
      if ((activeRoute.pb_time || 0) - attemptDuration.attempt < 0) {
        return this.s.redTextGradient;
      }
      return this.s.grayTextGradient;
    }

    return this.s.blueTextGradient;
  }

  renderTime() {
    if (!this.state.splitsData) {
      return null;
    }
    const {attemptDuration} = this.state.splitsData;
    return (
      <DurationTimer
        type="large"
        milliseconds={attemptDuration.attempt}
        activeAt={attemptDuration.attemptActiveAt}
      />
    );
  }

  renderPreviousSegment() {
    if (!this.state.splitsData) {
      return null;
    }

    if (!this.state.splitsData.attemptDuration) {
      return null;
    }

    const splits = this.state.splitsData.activeSplits;
    const attemptDuration = this.state.splitsData.attemptDuration;
    let attemptTime = 0;
    let pbTime = 0;
    let result = 0;

    for (let i = 0; i < splits.length; ++i) {
      const split = splits[i];
      const timePoint = attemptDuration.splits[split.id];

      if (split.active) {
        const lastSplit = splits[i - 1];
        if (!lastSplit) {
          break;
        }

        result = attemptTime - pbTime;
        break;
      }

      attemptTime += timePoint ? timePoint.milliseconds : 0;
      pbTime += split.pb_time || 0;
    }

    return (
      <Box sx={{display: 'flex', flexDirection: 'row'}}>
        <Typography
          variant="subtitle1"
          sx={{
            fontFamily: "'Fira Sans'",
            padding: 1,
          }}
        >
          Previous Segment:&nbsp;
        </Typography>
        <Typography
          variant="subtitle1"
          className={cx({
            [this.s.redTextGradient]: result >= 0,
            [this.s.greenTextGradient]: result < 0,
          })}
          sx={{
            fontFamily: 'timer',
            padding: 1,
            pl: 0,
          }}
        >
          <DurationTimer activeAt={0} milliseconds={result} showSign />
        </Typography>
      </Box>
    );

    return null;
  }

  renderPersonalBest() {
    if (!this.state.splitsData) {
      return null;
    }

    const {activeRoute} = this.state.splitsData;

    return (
      <Box sx={{display: 'flex', flexDirection: 'row'}}>
        <Typography
          variant="subtitle1"
          sx={{
            fontFamily: "'Fira Sans'",
            padding: 1,
          }}
        >
          Personal Best:&nbsp;
        </Typography>
        <Typography
          variant="subtitle1"
          className={this.s.blueTextGradient}
          sx={{
            fontFamily: 'timer',
            padding: 1,
            pl: 0,
          }}
        >
          <DurationTimer
            activeAt={0}
            milliseconds={activeRoute.pb_time || 0}
          />
        </Typography>
      </Box>
    );
  }

  renderLogo() {
    if (!this.state.splitsData) {
      return null;
    }

    let logoImage = '0';

    if (this.state.splitsData.pageSettings.viewerSettings['speed-run.html']) {
      const viewerSettings = this.state.splitsData.pageSettings.viewerSettings['speed-run.html'];

      if (
        viewerSettings.logo_image &&
        typeof viewerSettings.logo_image === 'string'
      ) {
        logoImage = `/api/configured-resource?absolute_path=${viewerSettings.logo_image}`;
      } else {
        return null;
      }
    }

    return (
      <img
        className={css`
          max-width: 58px;
          max-height: 58px;
          position: absolute;
          left: 8px;
          top: 0;
        `}
        src={logoImage}
      />
    );
  }

  render() {
    if (!this.state.splitsData) {
      return null;
    }

    const {css} = this.state.splitsData.pageSettings;

    return (
      <Paper>
        <style dangerouslySetInnerHTML={{__html: css}} />
        <Box
          sx={{
            display: 'grid',
            gridTemplateColumns: '1fr auto',
            gridAutoRows: 'min-content',
            width: this.state.width,
            height: this.state.height,
          }}
        >
          {this.renderSplits()}
        </Box>
        <Box
          sx={{textAlign: 'right', pr: 1, position: 'relative'}}
          className={this.getTimerColor()}
        >
          {this.renderLogo()}
          {this.renderTime()}
        </Box>
        <Box>
          {this.renderPreviousSegment()}
        </Box>
        <Box>
          {this.renderPersonalBest()}
        </Box>
      </Paper>
    );
  }
}

export const SpeedRunApp = withTheme<Theme, typeof SpeedRunAppImpl>(SpeedRunAppImpl);

ReactDOM.render(
  <ThemeProvider theme={theme}>
    <CssBaseline />
    <SpeedRunApp />
  </ThemeProvider>,
  document.querySelector('#root'),
);

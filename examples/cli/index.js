/* eslint-disable @typescript-eslint/no-var-requires */
const {connect} = require('@havenofcode/challenge-run-client');

/**
 * This example could be used in a server-side application
 */
async function printAndExit(host, port) {
  const client = await connect({host, port});

  await new Promise((resolve) => {
    client.on('splits-updated', (data) => {
      console.log('=============================');
      console.log(data.activeRoute.name);
      console.log('=============================');
      data.activeSplits.forEach(({name, active}) => {
        console.log(` * ${name} ${active ? '[active]' : ''}`);
      });

      client.disconnect();
      resolve();
    });
  });
}

module.exports = {printAndExit}

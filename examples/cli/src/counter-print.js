#!/usr/bin/env node
/* eslint-disable  @typescript-eslint/no-var-requires */
const {Command} = require('commander'); // include commander in git clone of commander repo
const program = new Command();
const {printAndExit} = require('..');

program
  .option('-p, --port <port>', 'port of challenge-run-client')
  .option('-h, --host <host>', 'host of challenge-run-client');

program.parse(process.argv);

const main = async () => {
  const {host, port} = program.opts();
  await printAndExit(host, port);
}

module.exports.main = main;
main().then((res) => res && console.log(res)).catch(console.error);

import {EventEmitter} from 'events';
import {io, Socket} from "socket.io-client";

/**
 * A single split containing counted data.
 */
export interface ISplitItem {
  /** unique value that changes at the start of every run. */
  id: number;
  /** display named. */
  name: string;
  /** amount of hits counted on the personal best attempt. */
  pb_hits: number | null;
  /** amount of time spent on the personal best attempt. */
  pb_time: number | null;
  /** current amount of hits received on the way to boss. */
  hits_way: number;
  /** current amount of hits received by boss. */
  hits_boss: number;
  /** true if tihs is current sellected split during an attempt. */
  active: boolean;
}

/**
 * Time point and duration info for a split.
 */
export interface SplitDuration {
  /** duration in milliseconds */
  milliseconds: number;
  /**
   * epoch time in milliseconds representing when this timer has started when
   * non-zero. Value of 0 indicates timer is stopped.
   */
  activeAt: number;
}

/**
 * Time point and duration info for each split and entire attempt and.
 */
export interface AttemptDuration {
  /** duration in milliseconds */
  attempt: number;
  /**
   * epoch time in milliseconds representing when this timer has started when
   * non-zero. Value of 0 indicates timer is stopped.
   */
  attemptActiveAt: number;
  /**
   * Map of time point and duration info indexed by id of ISplitItem on the
   * current attempt.
   */
  splits: {[key: number]: SplitDuration};
}

/**
 * Title and identifier for a collection of splits on an attempt.
 */
export interface IRoute {
  /** unique identifier for a collection of splits for every attempt. */
  id: number;
  /** name of a collection of these splits (ex. DS3 Any%) */
  name: string;
  /** personal best time */
  pb_time?: number;
  /** personal best hit count */
  pb_hits?: number;
}

export type IViewerSettingsJson = {[key: string]: string | number | null}

/**
 * Page settings that usually affect the appearance
 */
export interface IPageSettings {
  width: string;
  height: string;
  css: string;
  viewerSettings: {
    [appName: string]: IViewerSettingsJson;
  };
}

export interface BaseViewerSetting {
  label: string;
  helperText?: string;
}

export interface TextViewerSetting extends BaseViewerSetting {
  type: "text";
}

export interface NumberViewerSetting extends BaseViewerSetting {
  type: "number";
  max?: number;
  min?: number;
}

export interface TextareaViewerSetting extends BaseViewerSetting {
  type: "textarea";
}

export interface FileViewerSetting extends BaseViewerSetting {
  type: "file";
  extensions?: string[];
}

export interface IViewerSettingsConfiguration {
  [key: string]:
    TextViewerSetting |
    NumberViewerSetting |
    TextareaViewerSetting |
    FileViewerSetting;
}

export interface INotifyConfigurationAvailable {
  name: string;
  configuration: IViewerSettingsConfiguration;
}


/**
 * Complete set of data containing all available info about an attempt on a
 * particular point in time.
 */
export interface ISplitsData {
  /** version of websocket api from splits controller's package.json */
  api_version: string;
  /** Multiple split collections. */
  routes: IRoute[];
  /** Current active route to display on attempt */
  activeRoute: IRoute;
  /** All splits within the active route on attempt */
  activeSplits: ISplitItem[];
  /** Time point and duration info for an entire attempt. */
  attemptDuration: AttemptDuration;
  /** All splits containing the best time segment */
  bestSegments: ISplitItem[];
  /** Current config options in challenge run counter settings. */
  pageSettings: IPageSettings;
}

/**
 * Options to connect to challenge-run-counter websocket server.
 */
export interface ConnectOptions {
  /** descriptive unique name for your application */
  app_name: string;
  host: string;
  port: string;
  protocol?: string;
}

/**
 * Names indicating the type of payload received from challenge-run-counter
 * websocket server. Used internally by Client
 */
export type WebsocketServerEventType =
  'splits-updated' |
  'next-split' |
  'prev-split' |
  'decrement-count' |
  'increment-count' |
  'page-settings-update';

/**
 * Names for type of event emitted by Client
 *
 * - `splits-updated` Emitted when any data has changed
 * - `websocket-connect_error` Forwards `connect_error` from socket io library
 */
export type ClientEventType =
  /** when splits are updated */
  'splits-updated' |
  /** websocket connection error */
  'websocket-connect_error';

/* eslint-disable-next-line no-var */
declare var window: Window;

export enum ClientState {
  CLIENT_DISCONNECT = 'CLIENT_DISCONNECT',
  CLIENT_CONNECTING = 'CLIENT_CONNECTING',
  CLIENT_OK = 'CLIENT_OK',
}

declare global {
  interface Location {
    reload(forceGet?: boolean): void;
  }
}

/**
 * Client for communicating with challenge-run-counter websocket server. Provides
 * useful serialization of data with performance in mind. It automatically
 * reconnects when counter app is closed and reopened.
 *
 * ```javascript
 * const connectOptions = {host: 'localhost', port: '42069'}
 * const client = await ChallengeRunClient.connect(connectOptions);
 *
 * client.on('splits-updated', (data) => {
 *   console.log('splits data recieved');
 *   console.log('====================');
 *   console.log({activeRoute});
 *   console.log({activeSplits});
 *   console.log({attemptDuration});
 *   console.log({pageSettings});
 * })
 * ```
 */
export class Client extends EventEmitter {
  private data?: ISplitsData;
  public _state: ClientState = ClientState.CLIENT_CONNECTING;
  public debug = false;
  public static ioclient = io;
  private lastApiVersion?: string;
  public configuration?: IViewerSettingsConfiguration;

  get lastApiVersionConnected(): string | undefined {
    return this.lastApiVersion;
  }

  public set state(val: ClientState) {
    // todo: temporarily adding this to help debug user issues
    console.warn(new Error(`set state: ${val}`));
    // if (window.location.search.indexOf('debug') > 0) {
    //   console.warn(new Error(`set state: ${val}`));
    // }

    this._state = val;
  }

  public get state() {
    return this._state;
  }

  /**
   * @param socket A connected SocketIO instance.
   */
  constructor(public appName: string, public socket: Socket) {
    super();
    this.routeEvents();
    this.routePublishEvents();
  }

  private onSplitsUpdated(data: ISplitsData) {
    const splitsData = {...data}

    // keep pageSettings if exists
    if (this.data) {
      splitsData.pageSettings = {...this.data.pageSettings}
    }

    // this condition will check if a reconnected client is connected to a different
    // version of the api and was connected more than once. 2 things might happen
    // depending on weather client is in browser or nodejs.
    //
    // - *nodejs client*: process is terminated with exit code 69
    // - *browser client*: browser is reloaded using the forceGet parameter = true
    if (this.lastApiVersion && this.lastApiVersion !== splitsData.api_version) {
      console.warn(
        'new server version connected',
        {
          old: this.lastApiVersion,
          new: splitsData.api_version
        }
      );

      if (typeof window !== 'undefined') {
        console.warn(`OBS BROWSER PAGE RELOADING: ${new Date().toISOString()}`);
        window.location.reload(true);
      } else {
        console.warn(
          'terminating process: remote controller version has changed',
          splitsData.api_version
        );

        process.exit(69);
        return;
      }
    }

    this.lastApiVersion = splitsData.api_version;
    this.data = splitsData;
    this.emit('splits-updated', splitsData);
  }

  private onNextSplit(time: number) {
    if (!this.data) {
      return;
    }

    const data = {...this.data}

    for (let i = 0; i < this.data.activeSplits.length; ++i) {
      if (this.data.activeSplits[i].active) {
        if (this.data.activeSplits[i + 1]) {
          const oldSplit = this.data.activeSplits[i];
          const newSplit = this.data.activeSplits[i + 1];
          this.setNextActiveSplit(newSplit, oldSplit, time);
        }

        break;
      }
    }

    this.data = data;
    this.emit('splits-updated', data, 'next-split');
  }

  private onPrevSplit(time: number) {
    if (!this.data) {
      return;
    }

    const data = {...this.data}

    for (let i = 0; i < this.data.activeSplits.length; ++i) {
      if (this.data.activeSplits[i].active) {
        if (this.data.activeSplits[i - 1]) {
          const oldSplit = this.data.activeSplits[i];
          const newSplit = this.data.activeSplits[i - 1];
          this.setNextActiveSplit(newSplit, oldSplit, time);
        }

        break;
      }
    }

    this.data = data;
    this.emit('splits-updated', data, 'prev-split');
  }

  private onDecrementCount(name: 'hits_way' | 'hits_boss') {
    if (!this.data) {
      return;
    }

    const data = {...this.data}

    for (let i = 0; i < data.activeSplits.length; ++i) {
      const split = data.activeSplits[i];

      if (split.active && split[name] > 0) {
        split[name] -= 1;
        break;
      }
    }

    this.data = data;
    this.emit('splits-updated', data, 'decrement-count');
  }

  private onIncrementCount(name: 'hits_way' | 'hits_boss') {
    if (!this.data) {
      return;
    }

    const data = {...this.data}

    for (let i = 0; i < data.activeSplits.length; ++i) {
      const split = data.activeSplits[i];

      if (split.active && split[name] < 99) {
        split[name] += 1;
        break;
      }
    }

    this.data = data;
    this.emit('splits-updated', data, 'increment-count');
  }

  private onDisconnect() {
    switch (this.state) {
      case ClientState.CLIENT_DISCONNECT: {
        break;
      }
      case ClientState.CLIENT_OK: {
        this.state = ClientState.CLIENT_CONNECTING;
        break;
      }
      case ClientState.CLIENT_CONNECTING: {
        this.state = ClientState.CLIENT_CONNECTING;
        break;
      }
    }
  }

  private onServerRestartNow() {
    if (typeof window !== 'undefined') {
      console.warn('server-restart-now received, reloading with forceGet = true');
      window.location.reload(true);
    } else {
      this.emit('server-restart-now');
    }
  }

  private onServerShutdownNow() {
    console.warn('server-shutdown-now called');
    this.emit('server-shutdown-now');
  }

  private onConnectError(err: Error) {
    this.emit('websocket-connect_error', err, 'connect_error')
  }

  private onConnect() {
    this.state = ClientState.CLIENT_OK;
  }

  private onPageSettingsUpdate(newPageSettings: Partial<IPageSettings>) {
    console.log('on page settings update', newPageSettings);

    if (this.data) {
      // merge new page settings with existing page settings
      if (newPageSettings.viewerSettings) {
        const keys = Object.keys(newPageSettings.viewerSettings);
        for (let i = 0; i < keys.length; ++i) {
          const key = keys[i];

          newPageSettings.viewerSettings[key] = {
            ...(this.data.pageSettings.viewerSettings[key] || {}),
            ...newPageSettings.viewerSettings[key],
          }
        }
      } else {
        newPageSettings.viewerSettings = this.data.pageSettings.viewerSettings;
      }

      this.data.pageSettings = {
        ...this.data.pageSettings,
        ...newPageSettings,
        viewerSettings: {
          ...this.data.pageSettings.viewerSettings,
          ...(newPageSettings.viewerSettings || {}),
        },
      }

      this.emit('splits-updated', this.data, 'page-settings-update');
    }
  }

  private routePublishEvents() {
    super.on(
      'notify-configuration-available',
      (
        payload: Partial<INotifyConfigurationAvailable> & {configuration: IViewerSettingsConfiguration},
        callback?: ((err: Error) => void),
      ) => {
        this.configuration = payload.configuration;
        return this.socket.emit('notify-configuration-available', {name: this.appName, ...payload}, callback);
      }
    );
  }

  private routeEvents(socket: Socket = this.socket) {
    socket.io.on('reconnect', this.onReconnect.bind(this));
    socket.on('connect', this.onConnect.bind(this));
    socket.on('connect_error', this.onConnectError.bind(this));
    socket.on('splits-updated', this.onSplitsUpdated.bind(this));
    socket.on('next-split', this.onNextSplit.bind(this));
    socket.on('prev-split', this.onPrevSplit.bind(this));
    socket.on('decrement-count', this.onDecrementCount.bind(this));
    socket.on('increment-count', this.onIncrementCount.bind(this));
    socket.on('disconnect', this.onDisconnect.bind(this));
    socket.on('server-restart-now', this.onServerRestartNow.bind(this));
    socket.on('server-shutdown-now', this.onServerShutdownNow.bind(this));
    socket.on('page-settings-update', this.onPageSettingsUpdate.bind(this));
  }

  /**
   * @param eventType name of event
   * @param callback callback with split data and the websocket server event name.
   */
  on(
    eventType: ClientEventType,
    callback: (
      data: ISplitsData,
      reason?: WebsocketServerEventType
    ) => void
  ): this {
    return super.on(eventType, callback);
  }

  /**
   * Internal instance method to append duration and timepoint info on the
   * client before the server has enought time to persist data on sqlite
   * database. Timepoints represent `split_start`, and `split_stop`.
   *
   * @param newSplit the newly active split at this time point
   * @param oldSplit the newly inactive split at this time point
   * @param time epoch date of event in milliseconds
   */
  private setNextActiveSplit(
    newSplit: ISplitItem,
    oldSplit: ISplitItem,
    time: number
  ) {
    oldSplit.active = false;
    newSplit.active = true;
    const {data} = this;

    if (!data) {
      return;
    }

    if (data.attemptDuration.attemptActiveAt > 0) {
      const {attemptDuration} = data;
      const splitOldDuration = attemptDuration.splits[oldSplit.id];
      const splitNewDuration = attemptDuration.splits[newSplit.id];
      const splitsDurationMap = attemptDuration.splits;

      splitsDurationMap[oldSplit.id] = {
        activeAt: 0,
        milliseconds: splitOldDuration.milliseconds + (time - splitOldDuration.activeAt),
      }

      splitsDurationMap[newSplit.id] = {
        activeAt: time,
        milliseconds: splitNewDuration ? splitNewDuration.milliseconds : 0,
      }
    }
  }

  onReconnect() {
    if (this.configuration) {
      this.socket.emit('notify-configuration-available', {
        configuration: this.configuration,
      });
    }
  }

  /**
   * Disconnects, and does not attempt to reconnect.
   */
  disconnect() {
    console.warn('manual disconnect: not reconnecting now');
    this.state = ClientState.CLIENT_DISCONNECT;

    if (!this.socket.disconnected) {
      console.log('disconnect', new Error(), this.state);
      this.socket.disconnect();
    }
  }

  /**
   * Attempts to connect to challenge-run-counter websocket server, and returns
   * connected client. Failure to connect results in exception.
   */
  static async connect(opts?: ConnectOptions): Promise<Client> {
    let url = typeof opts !== 'undefined' ? `${opts.host}:${opts.port}` : '/';
    const appName = typeof opts !== 'undefined' ? opts.app_name : 'Unknown App Name';

    if (typeof window !== 'undefined' && window && !opts) {
      const portRegex = /port=[0-9]+/;
      const hostRegex = /host=([a-z]|[A-Z]|-|_|[0-9])+/;
      const {search} = window.location;
      const portMatch = portRegex.exec(search);
      const hostMatch = hostRegex.exec(search);

      if (portMatch && hostMatch) {
        const port = (portMatch[0] || '').split('=')[1];
        const host = (hostMatch[0] || '').split('=')[1];
        url = `${host}:${port}`;
      }
    }

    const socket = this.ioclient(`http://${url}`);
    const client = new Client(appName, socket);
    return Promise.resolve(client);
  }
}

/**
 * Connect to the websocket server
 * @param opts connection options
 */
export const connect = async (opts?: ConnectOptions): Promise<Client> => {
  return Client.connect(opts);
}

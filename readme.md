challenge-run-client
====================

Library for reading
[challenge-run-counter](https://gitlab.com/havenofcode/challenge-run-counter/app)
data. Can be used to create a speedrun or hit counter viewer in a browser, or
server-side application.

```
npm install --save @havenofcode/challenge-run-client
```

## Docs

All documentation is hosted
[here](https://havenofcode.gitlab.io/challenge-run-counter/client/).

## Development

The default Challenge Run Counter pages are provided by this npm module. If you
want to use this in your own project, installing through npm is recommended.

```
npm ci
npm run build
```

## Test

```
npm test
```

## Examples

- [node](./examples/cli/readme.md): cli to display in console
- [react](./src/examples/default.tsx): react app to display in a
browser
- [vanilla html](./examples/simple.html): plain html included in a
script
- [speed-run](./examples/speed-run.html): for speed runs

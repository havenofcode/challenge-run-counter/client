challenge-run-counter-cli
=========================

A simple CLI for printing challenge-run-counter display data in console. This
example could be used in a server-side application.

```
npm install
npm link
counter -h
```

```
Usage: counter [options] [command]

challenge-run-client cli example

Options:
  -V, --version   output the version number
  -h, --help      display help for command

Commands:
  print|p         prints out data and exits
  help [command]  display help for command
```

`src/counter-print.js`

Prints current challenge-run-counter data and exits.

```
counter print --host localhost --port 42069
```

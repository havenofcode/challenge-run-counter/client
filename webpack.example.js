const path = require('path');
const examplesDir = path.join(__dirname, '.webpack', 'examples');

module.exports = {
  entry: {
    default: path.join(examplesDir, 'default.js'),
    'speed-run': path.join(examplesDir, 'speed-run.js'),
  },
  mode: process.env.NODE_ENV === 'production' ? 'production' : 'development',
  devServer: {
    static: {
      directory: path.join(__dirname, 'examples'),
    },
    compress: true,
    port: 9000,
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /(node_modules|\.webpack)/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              presets: [
                ['@babel/preset-env', {
                  useBuiltIns: 'usage',
                  corejs: '3.18.3',
                }],
                '@babel/preset-react',
              ],
              plugins: [
                "@emotion/babel-plugin",
              ],
            },
          },
        ],
      },
    ],
  },
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'dist'),
  },
};

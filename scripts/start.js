/* eslint @typescript-eslint/no-var-requires: "off" */
const child_process = require('child_process');
const os = require('os');
const path = require('path');
const root = path.resolve(__dirname, '..');

const childProcessOptions = {
  cwd: root,
  stdio: 'inherit',
  env: {
    ...process.env,
    NODE_ENV: 'development',
  }
}

const waitUntilDone = async (args) => {
  const cmd = `> npx ${args.join(' ')}`;
  console.log(cmd);

  return new Promise((resolve, reject) => {
    const child = child_process.spawn(
      ...([os.platform() === 'win32' ? 'npx.cmd' : 'npx']),
      args,
      childProcessOptions
    );

    child.on('exit', (code) => {
      if (code) {
        reject(new Error(`error: ${cmd} exit with ${code}`));
      } else {
        console.info(`success: ${cmd}`);
        resolve(code);
      }
    });
  });
}

const prebuildClient = (watch = false) => waitUntilDone([
  'tsc',
  ...(watch ? ['-w', '--preserveWatchOutput'] : []),
  '-P',
  'tsconfig.example.json',
]);

const prebuildExamples = (watch = false) => waitUntilDone([
  'tsc',
  ...(watch ? ['-w', '--preserveWatchOutput'] : []),
  '-P',
  'tsconfig.json',
]);

const buildClient = (watch = false) => waitUntilDone([
  'webpack',
  '-c',
  'webpack.config.js',
  ...(watch ? ['--watch'] : []),
]);

const buildExampleWebpack = (watch = false) => waitUntilDone([
  'webpack',
  ...(watch ? ['serve'] : []),
  '-c',
  'webpack.example.js',
]);

const runFullBuild = async () => {
  await prebuildClient();
  await buildClient();
  await prebuildExamples();
  await buildExampleWebpack();
}

const main = async () => {
  await runFullBuild();

  await Promise.all([
    prebuildClient(true),
    buildClient(true),
    prebuildExamples(true),
    buildExampleWebpack(true),
  ]);
}

main().catch((err) => {
  console.error(err);
  process.exit(1);
});
